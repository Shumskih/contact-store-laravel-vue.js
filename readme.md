# Contact Store

Contact Manager application. Vue.js used as frontend and Laravel used as Backend.

##### Used Technologies:

- Laravel - PHP Framework
- Bootstrap - CSS Framework
- Vue.js - JavaScript Framework
- [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
